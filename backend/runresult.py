class RunResult:
    """
        Класс для хранения результата выполнения программы.
        
        result: Результат выполнения программы (например 'OK', 'RE', 'TL')
        stdout: stdout выполненной программы
        exit_code: код, который вернула программа
    """
    def __init__(self, result, stdout, exit_code):
        self.result = result
        self.stdout = stdout
        self.exit_code = exit_code

    def __repr__(self):
        return '{}({!r},{!r},{!r})'.format(self.__class__.__name__, self.result, self.stdout, self.exit_code)

    def is_successful(self):
        return self.result == "OK"
