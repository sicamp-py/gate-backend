import shutil
import subprocess
import config
import os
from typing import Union, List
from pathlib import Path

import backend.saferun as srun
from backend.runresult import RunResult

PathT = Union[Path, str]


class BaseEnv:
    def __init__(self, path: PathT, temp_dir=True):
        """
        :param temp_dir: env создаётся в временной директории, которая должна не существовать
        :param path: Путь к папке, где создать env.
        """
        self.temp_dir = temp_dir
        self.path = Path(path)

    def __enter__(self):
        """
            Про exist_ok: Если мы создаём в временной директории, то нам нужно проверить, что директория не существует,
            иначе мы можем удалить директорию с важными файлами при __exit__.
        """
        if self.temp_dir:
            self.path.mkdir()
        else:
            self.path.mkdir(exist_ok=True)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.temp_dir:
            shutil.rmtree(str(self.path))

    def _is_correct_path(self, path: Path) -> bool:
        """
        Проверить путь на корректность
        :param path: Путь, который надо проверить
        :return: 
        """
        return self.path in path.resolve().parents

    def _remove_object(self, real_path: Path):
        if real_path.is_dir():
            shutil.rmtree(real_path)
        else:
            os.remove(real_path)

    def _copy_object(self, old_path: Path, new_path: Path):
        """
        Скопировать объект (файл или директорию)
        :param old_path: Откуда
        :param new_path: Куда
        """
        if old_path.is_dir():
            shutil.copytree(old_path, new_path)
        else:
            shutil.copy2(old_path, new_path)

    def open(self, path_in_env: PathT, mode='r', **kwargs):
        """
        Открыть файл внутри env.
        :param path_in_env: Путь к файлу внутри env
        :param mode: Аналогично open
        :param kwargs: Дополнительные параметры к open
        :return: Аналогично open
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        return open(real_path, mode, **kwargs)

    def _get_absolute_path_in_env(self, path_in_env: PathT) -> Path:
        """
        Получить абсолютный путь объекта внутри env.
        Кидает ValueError, если результирующий путь находится не в env
        :param path_in_env: Путь объекта в env
        :return: Абсолютный путь к объекту
        """
        path = (self.path / path_in_env).resolve()
        if not self._is_correct_path(path):
            raise ValueError('Invalid path')
        return path

    def file_exists(self, path_in_env: PathT) -> bool:
        """
        Проверить существоване файла.
        :param path_in_env: Путь к файлу внутри env
        :return: bool
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        return real_path.is_file()

    def directory_exists(self, path_in_env: PathT) -> bool:
        """
        Проверить существование директории
        :param path_in_env: Путь к файлу внутри директории
        :return: bool
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        return real_path.is_dir()

    def exists(self, path_in_env: PathT) -> bool:
        """
        Проверить существование объекта (файла или директории)
        :param path_in_env: Путь к объекту внутри env
        :return: bool
        """
        return self.file_exists(path_in_env) or self.directory_exists(path_in_env)

    def copy_to(self, path_in_fs: PathT, path_in_env: PathT):
        """
        Скопировать файл из фс в env.
        :param path_in_fs: Путь в фс
        :param path_in_env: Путь файла в env
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        self._copy_object(path_in_fs, real_path)

    def copy_from(self, path_in_env: PathT, path_in_fs: PathT):
        """
        Скопировать файл из env в фс.
        :param path_in_env: Путь файла в env
        :param path_in_fs: Путь файла в фс
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        self._copy_object(real_path, path_in_fs)

    def copy_in(self, path_in_env_from: PathT, path_in_env_to: PathT):
        """
        Скопировать файл из env в env
        :param path_in_env_from: Откуда
        :param path_in_env_to: Куда
        """
        real_path_from = self._get_absolute_path_in_env(path_in_env_from)
        real_path_to = self._get_absolute_path_in_env(path_in_env_to)

        if not real_path_to.samefile(real_path_from):
            self._copy_object(real_path_from, real_path_to)

    def move_to(self, path_in_fs: PathT, path_in_env: PathT):
        """
        Переместить файл из фс в env
        :param path_in_fs: Путь в фс
        :param path_in_env: Путь файла в env
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        shutil.move(path_in_fs, real_path)

    def move_from(self, path_in_env: PathT, path_in_fs: PathT):
        """
        Переместить файл из env в фс.
        :param path_in_env: Путь файла в env
        :param path_in_fs: Путь файла в фс
        """
        real_path = self._get_absolute_path_in_env(path_in_env)
        shutil.move(real_path, path_in_fs)

    def move_in(self, path_in_env_from: PathT, path_in_env_to: PathT):
        """
        Переместить файл из env в env
        :param path_in_env_from: Откуда
        :param path_in_env_to: Куда
        """
        real_path_from = self._get_absolute_path_in_env(path_in_env_from)
        real_path_to = self._get_absolute_path_in_env(path_in_env_to)

        if not real_path_to.samefile(real_path_from):
            shutil.move(real_path_from, real_path_to)

    def remove(self, path_in_env: PathT):
        real_path = self._get_absolute_path_in_env(path_in_env)
        self._remove_object(real_path)

    def cleanup(self):
        """
        Очистить env.
        """
        pass

    def run(self, args: List[str], cwd=None, **limits):
        """
        Запустить программу внутри env
        :param limits: Ограничения
        :param args: Программа и аргументы
        :param cwd: Путь, в котором запускаем
        """
        pass


class StupidEnv(BaseEnv):
    def run(self, args: List[str], cwd=None, **limits):
        if cwd is None:
            cwd = ''
        proc = subprocess.run(args,
                              cwd=str(self.path / cwd),
                              stdout=subprocess.PIPE)
        result = 'OK'
        if proc.returncode != 0:
            result = 'RE'
        return RunResult(result, proc.stdout, proc.returncode)


class SafeRunEnv(BaseEnv):
    def __enter__(self):
        if self.temp_dir:
            self.path.mkdir()
        else:
            self.path.mkdir(exist_ok=True)
        subprocess.run([config.saferun_config['env_helper_path'], 'create', str(self.path.resolve())])
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        subprocess.run([config.saferun_config['env_helper_path'], 'remove', str(self.path.resolve())])
        if self.temp_dir:
            shutil.rmtree(str(self.path))

    def run(self, args: List[str], cwd=None, **limits):
        proc = srun.invoke_saferun(
            args,
            chroot=str(self.path),
            chdir=cwd,
            **limits)

        # Report format
        # SRUN_REPORT: {string_result} {result} {time} {real_time} {mem} {status} {exit_code_or_string_description_for_signal}
        report = proc.stderr.decode('utf-8').strip().splitlines()[-1]
        assert report.startswith('SRUN_REPORT: ')
        _, result, time, real_time, mem, returncode = report.split(' ', 5)

        return RunResult(result, proc.stdout, int(returncode))
