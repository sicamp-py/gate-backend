import time
import logging

import requests.exceptions

from backend.gateapi import TestingResult
from .gateapi import Solution, GateApiClient
from .solutionhandler import SolutionHandler
import config


class TestingBackend:
    def __init__(self, gate_api: GateApiClient, delay_between_requests=1.0):
        self.gate = gate_api
        self.delay_between_requests = delay_between_requests

    def main_loop(self):
        logging.info('Resetting gate status...')
        self.gate.reset_status()
        while True:
            try:
                tasks = self.gate.get_task_list()
                logging.debug('Got task list ' + str(tasks))
                if len(tasks):
                    tid, lid = tasks[0]
                    self.process_task(tid, lid)
                else:
                    time.sleep(self.delay_between_requests)
            except requests.exceptions.ConnectionError:
                logging.error('Can\'t connect to frontend', exc_info=True)
                time.sleep(self.delay_between_requests)
            except Exception as e:
                logging.critical('Uncaught exception in main_loop', exc_info=True)
                time.sleep(self.delay_between_requests)

    def process_task(self, tid, lid):
        try:
            logging.info('Processing task {} {}'.format(tid, lid))
            self.gate.delete_task(tid, lid)
            task = self.gate.get_task(tid, lid)
            solution = Solution.from_data(tid, task)
            solution_handler = SolutionHandler(solution, config)
            testing_result = solution_handler.test_solution()
            self.gate.put_solution(tid, lid, testing_result)
        except Exception as e:
            msg = 'Uncaught exception in process_task'
            logging.critical(msg, exc_info=True)
            testing_result = TestingResult()
            testing_result.cr('{}: {}'.format(msg, str(e)))
            self.gate.put_solution(tid, lid, testing_result)
