import logging
import os
import shutil
from pathlib import Path

from backend.uploading_backend import parse_checker_conf
from .environment import RunResult
from .environment import SafeRunEnv as Env
from .gateapi import Solution, TestingResult


class SolutionHandler:
    """
    Main solution handler
    """

    def __init__(self, solution: Solution, config):
        self.solution = solution
        self.testing_dir = Path(config.dirs['testing_dir']) / str(solution.id)
        self.problem_dir = Path(config.dirs['tests_dir']) / str(solution.problem_id)
        self.compiler_config = config.solution_compilers[solution.compiler_id]

        self.testing_dir.mkdir(parents=True, exist_ok=True)
        self.config = config

        if (self.testing_dir / 'testing_env').exists():
            shutil.rmtree(self.testing_dir / 'testing_env')

        self.testing_env = Env(self.testing_dir / 'testing_env', temp_dir=False)
        self.testing_limits = {
            'time_limit': solution.time_limit,
            'real_time_limit': solution.time_limit*2,
            'memory_limit': solution.memory_limit,
            'namespaces': True,
            'seccomp': True,
        }
        self.checker_env = Env(self.testing_dir / 'checker_env', temp_dir=False)
        self.checker_limits = config.default_limits['checker']
        self.compile_env = Env(self.testing_dir / 'compile_env', temp_dir=False)
        self.compiler_limits = dict(config.default_limits['compiler'])
        if 'limits' in self.compiler_config:
            self.compiler_limits.update(self.compiler_config['limits'])

        self.solution_path = self.testing_dir / \
                             self.compiler_config['solution_filename']

        # Load checker config
        with open(self.problem_dir / 'checker.conf') as f:
            checker_conf = parse_checker_conf(f)

        if 'CheckerID' in checker_conf:
            checker_basepath = Path(self.config.dirs['checkers_dir']) / checker_conf['CheckerID']
            with open(checker_basepath / 'checker.conf') as f:
                checker_conf = parse_checker_conf(f)
        else:
            checker_basepath = self.problem_dir

        checker_compiler = self.config.checker_toolchains[checker_conf['CompilerID']]

        self.checker = dict(self.config.checker)
        self.checker['checker_filename'] = checker_compiler['checker_filename']
        self.checker['checker_command'] = checker_compiler['run'] + self.checker['checker_args']
        self.checker_path = checker_basepath / checker_compiler['checker_filename']

    def compile_solution(self) -> TestingResult:
        """
        This method allows you to compile solution if it is compilable
        """
        logging.debug('Пытаемся скомпилировать решение...')
        with self.compile_env as env:
            with env.open(self.compiler_config['source_filename'], 'w') as f:
                f.write(self.solution.source)

            run_result = env.run(self.compiler_config['compile'], **self.compiler_limits)
            if not run_result.is_successful():
                logging.debug('Не получается скомпилировать решение. CE')
                result = TestingResult()
                result.ce(run_result.stdout)
                return result
            else:
                logging.debug('Решение успешно скомпилировано')

            env.move_from(self.compiler_config['solution_filename'],
                          self.solution_path)

        return TestingResult()

    def test_solution(self) -> TestingResult:
        """
        Main method of the class.
        It starts compiler if avilable, run on all tests and return result
        """
        if self.compiler_config['compile']:
            compile_result = self.compile_solution()
            if compile_result.compilation_error:
                return compile_result
        else:
            with open(self.solution_path, 'w') as f:
                f.write(self.solution.source)

            logging.debug("Это решение не нужно компилировать.")

        with self.testing_env, self.checker_env:

            self.testing_env.move_to(
                self.solution_path, self.compiler_config['solution_filename'])

            self.checker_env.copy_to(
                self.checker_path, self.checker['checker_filename'])
            logging.debug('Файл решения и чекер к нему скопированы.')
            fault = False
            testing_result = TestingResult()
            try:
                tests = self.get_task_tests(self.problem_dir)
                assert len(tests) >= len(self.solution.tests),\
                    "Слишком мало тестов в архиве ({}, ожидается как минимум {})".format(
                        len(tests), len(self.solution.tests))
            except (FileNotFoundError, ValueError, AssertionError) as e:
                logging.warning('Wrong problem\'s structure!', exc_info=True)
                testing_result.cr(str(e))
                return testing_result

            for i, points in enumerate(self.solution.tests):
                logging.debug('Testing {}st test'.format(i + 1))
                result, checker_output = self.test_on_test(tests[i])

                testing_result.add_test(result, i, checker_output)

                if result == 'OK':
                    testing_result.points += points
                else:
                    fault = True
                    if self.solution.acm:
                        logging.debug('Режим АСМ, выходим из цикла проверки на тестах...')
                        break

            if not fault:
                testing_result.points += self.solution.bonus

            return testing_result

    @staticmethod
    def get_task_tests(problem_dir: Path):
        """
        Возвращет массив кортежей вида: (файл теста, файл ответа)
        Путь до файлов - относительный от папки с задачей
        :param problem_dir: Папка с файлами тестов и ответов
        :return массив кортежей
        """

        directory_content = os.listdir(str(problem_dir))

        def is_testfile(file):
            return file.endswith('.tst') or file.endswith('.ans')

        def find_test_files(problem_dir):
            files = os.listdir(str(problem_dir))
            return (file for file in files if is_testfile(file))

        def filter_invalid_names(files):
            for file in files:
                name = file.rsplit('.', 1)[0]
                if name.isdigit():
                    yield file
                else:
                    logging.warning('Неправильное имя файла {}!'.format(file))

        def check_tests_have_valid_numbers(tests):
            for i, (input, answer) in enumerate(tests, start=1):
                if input.rsplit('.', 1)[0].lstrip('0') != str(i):
                    raise FileNotFoundError('Нет теста с номером {}'.format(str(i)))
                if input.rsplit('.', 1)[0] != answer.rsplit('.', 1)[0]:
                    raise FileNotFoundError(
                        'Название теста и ответа не совпадают\n Тест: {} Ответ: {}'.format(input, answer))

        def check_tests_are_files(tests):
            for (input, answer) in tests:
                if not os.path.isfile(problem_dir / input) or not os.path.isfile(problem_dir / answer):
                    raise FileNotFoundError("Тест {} не является файлом".format(input))

        def natural_key(file):
            return int(file.rsplit('.', 1)[0])

        test_files = find_test_files(problem_dir)
        valid_test_files = sorted(filter_invalid_names(test_files), key=natural_key)

        inputs = [file for file in valid_test_files if file.endswith('.tst')]
        answers = [file for file in valid_test_files if file.endswith('.ans')]

        if len(inputs) != len(answers):
            logging.warning('Количество тестов не совпадает с количеством ответов!')
            raise FileNotFoundError("Количество тестов: : {}\nКоличество ответов: {}".format(len(inputs), len(answers)))

        tests = list(zip(inputs, answers))

        check_tests_have_valid_numbers(tests)
        check_tests_are_files(tests)

        return tests

    def test_on_test(self, test: tuple) -> (str, str):
        """
        This function test the solution on some examples
        """
        RETURN_CODE_MAP = {
            0: 'OK',
            1: 'WA',
            2: 'PE',
            4: 'CR',
            8: 'FAIL'
        }

        test_input_path = self.problem_dir / test[0]
        test_answer_path = self.problem_dir / test[1]
        result = RunResult(None, None, None)
        if not os.path.exists(test_input_path):
            return 'CR', result.stdout
        self.testing_env.copy_to(test_input_path, self.solution.input_file)
        result = self.testing_env.run(self.compiler_config['run'], **self.testing_limits)
        if not result.is_successful():
            logging.debug('Result was not successful. %s %s ',
                          result.result, result.stdout)
            return result.result, None

        if not self.testing_env.file_exists(self.solution.output_file):
            logging.debug('No output file %s from solution',
                          self.checker['output_file'])
            return 'PE', 'No output file'
        self.testing_env.move_from(
            self.solution.output_file, self.testing_dir / self.checker['output_file'])

        try:
            result = self._run_checker(test_input_path,
                                       self.testing_dir / self.checker['output_file'],
                                       test_answer_path)
        except Exception as e:
            logging.error('Не удалось запустить чекер!! {}'.format(str(e)))
            return 'CR', "Couldn't run checker"

        return RETURN_CODE_MAP[result.exit_code], result.stdout

    def _run_checker(self, test_input_path, solution_output_path, test_answer_path):
        self.checker_env.move_to(solution_output_path, self.checker['output_file'])
        self.checker_env.copy_to(test_input_path, self.checker['input_file'])
        self.checker_env.copy_to(test_answer_path, self.checker['answer_file'])

        logging.debug('Запуск чекера...')
        result = self.checker_env.run(self.checker['checker_command'], **self.checker_limits)
        return result
