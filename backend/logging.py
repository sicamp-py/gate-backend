import logging

def start_logging():
    logging.basicConfig(
        format='%(levelname)-8s [%(asctime)s] %(message)s', level=logging.DEBUG)
    logging.getLogger("requests").setLevel(logging.DEBUG)
    # logging.disable(logging.DEBUG)
