from config import saferun_config
import subprocess
import os


def mount(path, dirname):
    os.system('mkdir -p {}'.format(path + dirname))
    os.system('mount -o bind,ro {} {}'.format(dirname, path + dirname))


def build_chroot(path):
    os.system('mkdir -p {}'.format(path))
    mount(path, '/bin')
    mount(path, '/lib')
    if os.path.exists('/lib64'):
        mount(path, '/lib64')
    mount(path, '/usr')
    os.system('mkdir -p {}'.format(os.path.join(path, 'tmp')))


def invoke_saferun(exec, memory_limit=None, time_limit=None, real_time_limit=None,
                   namespaces=True, seccomp=False, chroot=None, chdir=None):
    args = [saferun_config['binary_path']]
    if memory_limit:
        args.extend(['--mem', str(int(memory_limit))])
    if time_limit:
        args.extend(['--time', str(int(time_limit))])
    if real_time_limit:
        args.extend(['--real-time', str(int(real_time_limit))])
    if namespaces:
        args.extend(['--usens', '1'])
    if seccomp:
        args.extend(['--seccomp', '1'])
    if chroot:
        args.extend(['--chroot', chroot])
    if chdir:
        args.extend(['--chdir', chdir])

    args.extend(['--redirect-stderr', 'stdout'])
    args.extend(exec)
    print(' '.join(args))
    return subprocess.run(args,
                         shell=False,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)