import logging
import os
import shutil
import time
from pathlib import Path

import requests.exceptions

import config
from backend.environment import StupidEnv as Env
from backend.gateapi.problem import Problem
from .gateapi import GateApiClient


def parse_checker_conf(file):
    checker_conf = {}
    for s in file.readlines():
        key, value = s.strip().split(maxsplit=1)
        checker_conf[key] = value
    return checker_conf


class UploadingBackend:
    def __init__(self, gate_api: GateApiClient, gate_archive_dir, tests_dir, checkers_dir,
                 delay_between_requests=1.0):
        self.gate = gate_api
        self.delay_between_requests = delay_between_requests
        self.gate_archive_dir = Path(gate_archive_dir)
        self.tests_dir = Path(tests_dir)
        self.checkers_dir = checkers_dir

    @staticmethod
    def _unpack_archive(what: Path, where_to: Path):
        if not what.is_file():
            raise FileNotFoundError('{} is not a file'.format(str(what)))
        shutil.unpack_archive(str(what), str(where_to))

    def _unpack_problem(self, problem, to_where):
        path_to_archive = self.gate_archive_dir / problem.filename
        if to_where.is_dir():
            shutil.rmtree(str(to_where))
        to_where.mkdir()
        self._unpack_archive(path_to_archive, to_where)

    def _compile_checker(self, env, checker_path, checker_source_path, compiler, cwd=Path('')):
        env.copy_in(cwd / checker_source_path, cwd / compiler['source_filename'])
        if compiler['compile']:
            run_result = env.run(compiler['compile'], cwd=cwd)
            if run_result.is_successful():
                env.remove(cwd / compiler['source_filename'])  # we remove copied file
                env.move_in(cwd / compiler['checker_filename'], cwd / checker_path)
            return run_result

    def _make_checker(self, env, path_to_problem):
        checker_conf_path = path_to_problem / 'checker.conf'
        if env.file_exists(checker_conf_path):
            with env.open(checker_conf_path) as f:
                checker_conf = parse_checker_conf(f)
            compiler = config.checker_toolchains[checker_conf['CompilerID']]
            self._compile_checker(env,
                                  'check',
                                  checker_conf['SourceFile'],
                                  compiler,
                                  cwd=path_to_problem)

    def _ensure_directories_exist(self):
        if not self.gate_archive_dir.is_dir():
            raise FileNotFoundError('Gate upload storage directory does not exist')
        os.makedirs(str(self.tests_dir), exist_ok=True)

    def main_loop(self):
        self._ensure_directories_exist()

        while True:
            try:
                checker = self.gate.get_checker()
                if checker:
                    self.upload_checker(checker)
                    continue

                problem = self.gate.get_problem()
                if problem:
                    self.upload_problem(Problem.from_data(problem))
                    continue
            except requests.exceptions.ConnectionError:
                logging.error('Can\'t connect to frontend', exc_info=True)
            except Exception as e:
                logging.critical('Uncaught exception in uploader\'s main_loop', exc_info=True)

            time.sleep(self.delay_between_requests)

    def upload_checker(self, checker):
        id, src, compilerid = checker['ID'], checker['SRC'], checker['COMPILERID']
        compiler = config.checker_toolchains.get(compilerid)
        if not compiler:
            msg = 'Unsupported compiler for checkers: ' + compilerid
            logging.info(msg)
            self.gate.put_checker(id, False, message=msg)
            return
        try:
            working_dir = Path(self.checkers_dir)
            checker_dir = working_dir / id
            checker_dir.mkdir(parents=True, exist_ok=True)
            checker_source_path = checker_dir / compiler['source_filename']
            checker_source_path.write_text(src)

            # FIXME source_filename, solution_filename
            with Env(working_dir / 'compile_env', temp_dir=False) as env:
                env.copy_to(checker_source_path, 'checker_src')
                run_result = self._compile_checker(env, 'check', 'checker_src', compiler)
                if run_result.is_successful():
                    env.move_from('check', checker_dir / compiler['checker_filename'])
                    (checker_dir / 'checker.conf').write_text('CompilerID {}'.format(compilerid))
                    self.gate.put_checker(id)
                else:
                    logging.info('Error while compiling checker {}'.format(id))
                    self.gate.put_checker(id, success=False, message=run_result.stdout)
        except Exception as e:
            msg = 'Uncaught exception in upload_checker'
            logging.critical(msg, exc_info=True)
            self.gate.put_checker(id, success=False, message='{}: {}'.format(msg, str(e)))

    def upload_problem(self, problem):
        try:
            if problem.filename:
                self.upload_problem_archive(problem)
            if problem.standard_checker:
                self.set_standard_checker(problem)
            self.gate.put_problem(problem.p_id)
        except Exception as e:
            self.gate.put_problem(problem.p_id, False,
                                  message=str(e))

    def upload_problem_archive(self, problem):
        try:
            self._unpack_problem(problem, self.tests_dir / problem.p_id)
            with Env(self.tests_dir / 'compile_env') as env:
                env.move_to(self.tests_dir / problem.p_id, 'problem')
                self._make_checker(env, Path('problem'))
                env.move_from('problem', self.tests_dir / problem.p_id)
            logging.info('problem {} successfully loaded'.format(problem.p_id))
        except shutil.ReadError as e:
            msg = 'Invalid or unsupported archive: ' + problem.filename
            logging.info(msg)
            raise Exception(msg) from e
        except FileNotFoundError as e:
            msg = 'Invalid archive path, probably problem in config: ' + problem.filename
            logging.warning(msg)
            raise Exception(msg) from e
        except Exception as e:
            logging.critical('Unknown error in upload_problem_archive', exc_info=True)
            raise

    def set_standard_checker(self, problem):
        with open(self.tests_dir / problem.p_id / 'checker.conf', 'w') as f:
            f.write('CheckerID {}'.format(problem.standard_checker))

