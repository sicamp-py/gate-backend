
import subprocess

PIPE=-1
STDOUT='stdout'
DEVNULL='null'

SRUN2_PATH='/home/xelez/programming/saferun2/srun2/srun2'

def srun2(*cmd_args,
        time_limit=None, mem_limit=None, real_time_limit=None,
        chroot=None, cwd=None,
        stdin=None, stdout=None, stderr=STDOUT,
        seccomp=False, namespaces=False):

    srun2_args = [SRUN2_PATH]

    if time_limit is not None:
        srun2_args.extend(['--time', str(time_limit)])

    if real_time_limit is not None:
        srun2_args.extend(['--real_time', str(real_time_limit)])

    if mem_limit is not None:
        srun2_args.extend(['--mem', str(mem_limit)])

    if chroot is not None:
        srun2_args.extend(['--chroot', chroot])

    if cwd is not None:
        srun2_args.extend(['--chdir', cwd])

    if seccomp:
        srun2_args.extend(['--seccomp', 'on'])

    if namespaces:
        srun2_args.extend(['--usens', 'on'])

    if stdin is not None and stdin != PIPE:
        srun2_args.extend(['--redirect-stdin', stdin])

    if stdout is not None and stdout != PIPE:
        srun2_args.extend(['--redirect-stdout', stdout])

    if stderr is None or stderr == PIPE:
        raise ValueError("stderr can only be path to file or DEVNULL or STDOUT")

    srun2_args.extend(['--redirect-stderr', stderr])

    subprocess.run(*srun2_args,
                   shell=False,
                   stdout=(subprocess.PIPE if stdout==PIPE else None),
                   stdin=(subprocess.PIPE if stdin == PIPE else None),

                   )
