class Solution:
    def __init__(self, s_id, acm, bonus, compiler_id, input_file, output_file, problem_id,
                 source, memory_limit, time_limit, tests):
        self.id = s_id
        self.acm = acm
        self.bonus = bonus
        self.compiler_id = compiler_id
        self.input_file = input_file
        self.output_file = output_file
        self.problem_id = problem_id
        self.source = source
        self.memory_limit = memory_limit * 1024  # in kbytes
        self.time_limit = time_limit * 1000  # in milliseconds
        self.tests = list(map(int, tests))

    @classmethod
    def from_data(cls, s_id, data):
        return Solution(
            s_id,
            data.get('ACM', 'false').lower() == 'true',
            int(data.get('BONUS', '0')),
            data['COMPILERID'],
            data['INPUTFILE'],
            data['OUTPUTFILE'],
            data['PROBLEMID'],
            data['SOURCE'],
            float(data.get('MEMORYLIMIT')),
            float(data.get('TIMELIMIT')),
            data['TESTS'].split())
