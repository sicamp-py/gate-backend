class Problem:
    def __init__(self, p_id, filename=None, standard_checker=None):
        self.p_id = p_id
        self.filename = filename
        self.standard_checker = standard_checker

    @classmethod
    def from_data(cls, data):
        return cls(p_id=data['ID'],
                   filename=data.get('FILENAME'),
                   standard_checker=data.get('CHECKER'))
