import requests

from .utils import decode_array, encode_list


def format_testing_result_as_dict(result):
    all_test_errors = [test[0] for test in result.tests]
    outputs = [test[1] for test in result.tests]
    checker_outputs = [test[2] for test in result.tests]

    errors = set(all_test_errors)
    if result.compilation_error:
        errors.add('CE')
    if result.system_crash:
        errors.add('CR')
    if errors != {'OK'}:
        errors -= {'OK'}

    return {
        'POINTS': result.points,
        'COMPILER_MESSAGES': result.compiler_message,
        'ERRORS': ' '.join(errors),
        'TESTS': ' '.join(all_test_errors),
        'SOLUTION_OUTPUT': encode_list(outputs),
        'CHECKER_OUTPUT': encode_list(checker_outputs),
        'REPORT': result.report,
    }


class GateApiClient:
    def __init__(self, tester_url, login, pass1, pass2):
        self.tester_url = tester_url
        self.auth_params = {
            'login': login,
            'pass1': pass1,
            'pass2': pass2
        }

    def _api_call(self, method, act, data=None, **kwargs):
        params = dict(self.auth_params)
        params['ipc'] = act
        params.update(kwargs)
        response = requests.request(method, self.tester_url, params=params, data=data)
        # TODO: raise exception
        return response

    def _get(self, act, **kwargs):
        return self._api_call('GET', act, data=None, **kwargs)

    def _post(self, act, data=None, **kwargs):
        return self._api_call('POST', act, data=data, **kwargs)

    def reset_status(self):
        """Run before starting mainloop, clears up testing queue state"""
        r = self._get('reset_status')
        return r.status_code == 200  # TODO: must return just None and errors are exceptions

    def get_task_list(self):
        """Gets testing queue from gate. Use `delete_task` to mark tasks as added to the queue."""
        r = self._get('get_task_list')
        if r.status_code != 200:
            return None
        return [row.split('@') for row in r.text.strip().split()]

    def get_task(self, id, module=0):
        """Returns task parameters in a dict"""
        r = self._get('get_task', id=id, lid=module)
        if r.status_code != 200:
            return None
        return decode_array(r.content)

    def delete_task(self, id, module=0):
        """Deletes task from Gate's queue (Marks as added to the internal queue)"""
        r = self._get('delete_task', id=id, lid=module)
        return r.status_code == 200

    def restore_task(self, id, module=0):
        """Restores task in Gate's queue. (Marks as not tested)"""
        r = self._get('restore_task', id=id, lid=module)
        return r.status_code == 200

    def get_checker(self):
        r = self._get('get_checker')
        if r.status_code != 200:
            return None
        if not r.content.strip():
            return None
        return decode_array(r.content)

    def get_problem(self, module=0):
        r = self._get('get_problem', lid=module)
        if r.status_code != 200:
            return None
        if not r.content.strip():
            return None
        return decode_array(r.content)

    def put_checker(self, id, success=True, message=''):
        data = {
            'err': 'OK' if success else 'FAIL',
            'desc': message,
        }
        r = self._post('put_checker', id=id, data=data)
        return r.status_code == 200

    def put_problem(self, id, success=True, message='', module=0):
        data = {
            'err': 'OK' if success else 'FAIL',
            'desc': message,
        }
        r = self._post('put_problem', id=id, lid=module, data=data)
        return r.status_code == 200

    def put_solution(self, id, module, testing_result):
        r = self._post('put_solution', id=id, lid=module, data=format_testing_result_as_dict(testing_result))
        return r.status_code == 200
