class TestingResult:
    def __init__(self):
        self.points = 0
        self.compiler_message = ''
        self.tests = []
        self.compilation_error = False
        self.system_crash = False
        self.report = ''
    def add_test(self, result, solution_output, checker_output):
        self.tests.append( (result, solution_output, checker_output) )

    def ce(self, compiler_message):
        self.compilation_error = True
        self.compiler_message = compiler_message
    def cr(self, report):
        self.system_crash = True
        self.report = report