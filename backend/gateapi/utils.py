def decode_array(input):
    data = {}

    while len(input):
        key, value_len, input = input.split(b';', 2)
        value = input[0:int(value_len)]
        data[key.decode()] = value.decode()

        input = input[int(value_len)+1:]

    return data


def encode_array(data):
    output = ''
    for key, val in data.items():
        val = str(val)
        output += '{0};{1};{2};'.format(key, len(val.encode()), val)
    return output


def encode_list(data):
    output = ''
    for key, val in enumerate(data):
        val = str(val)
        output += '{0};{1};{2};'.format(key, len(val.encode()), val)
    return output