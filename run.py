from backend.gateapi import GateApiClient
from backend.backend import TestingBackend
from backend.logging import start_logging


def make_backend(config) -> TestingBackend:
    gate_api = GateApiClient(config.gate['tester_url'],
                             config.gate['login'],
                             config.gate['pass1'],
                             config.gate['pass2'])

    return TestingBackend(gate_api, config.DELAY_BETWEEN_REQUESTS)


if __name__ == '__main__':
    import config
    start_logging()
    backend = make_backend(config)
    backend.main_loop()
