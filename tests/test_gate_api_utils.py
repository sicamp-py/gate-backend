from backend.gateapi.utils import decode_array, encode_array, encode_list


def test_decode_array_unicode():
    k = decode_array('source;12;аыуапу;hid;3;123;'.encode())
    assert k['source'] == 'аыуапу'
    assert k['hid'] == '123'


def test_encode_array_unicode():
    k = encode_array({'abc': 'фввф', 'lol': 2})
    assert k == 'abc;8;фввф;lol;1;2;'


def test_encode_list_unicode():
    k = encode_list(['фввф', 'lol'])
    assert k == '0;8;фввф;1;3;lol;'
