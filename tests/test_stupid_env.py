from backend.environment import StupidEnv
from pathlib import Path
import pytest


@pytest.fixture()
def stupid_env(tmpdir):
    path = Path(tmpdir)
    return StupidEnv(path / 'stupid_env')


def test_stupid_env_runs_applications(stupid_env):
    with stupid_env as env:
        run_result = env.run(['python', '-c', 'exit(0)'])
        assert run_result.is_successful()


def test_stupid_env_should_return_unsuccessful_result_if_app_returned_non_zero_code(stupid_env):
    with stupid_env as env:
        run_result = env.run(['python', '-c', 'exit(1)'])
        assert not run_result.is_successful()


def test_stupid_env_copies_files_to(tmpdir, stupid_env):
    tmpdir = Path(tmpdir)
    file = tmpdir / 'file'
    with stupid_env as env:
        file.touch()
        env.copy_to(file, 'file')
        assert env.file_exists('file')


def test_stupid_env_copies_files_from(tmpdir, stupid_env):
    tmpdir = Path(tmpdir)
    file = tmpdir / 'file'
    new_file = tmpdir / 'new_file'
    with stupid_env as env:
        file.touch()
        env.copy_to(file, 'file')
        env.copy_from('file', new_file)
        assert new_file.exists()


def test_stupid_env_moves_files_to(tmpdir, stupid_env):
    tmpdir = Path(tmpdir)
    file = tmpdir / 'file'
    with stupid_env as env:
        file.touch()
        env.move_to(file, 'file')
        assert env.file_exists('file')
        assert not file.exists()


def test_stupid_env_moves_files_from(tmpdir, stupid_env):
    tmpdir = Path(tmpdir)
    file = tmpdir / 'file'
    new_file = tmpdir / 'new_file'
    with stupid_env as env:
        file.touch()
        env.move_to(file, 'file')
        env.move_from('file', new_file)
        assert new_file.exists()
        assert not env.file_exists('file')


def test_stupid_env_opens_files(tmpdir, stupid_env):
    tmpdir = Path(tmpdir)
    file = tmpdir / 'file'
    with open(file, 'w') as f:
        f.write('kokokoko')
    with stupid_env as env:
        env.move_to(file, 'file')
        with env.open('file') as f:
            k = f.read()
            assert k == 'kokokoko'


def test_stupid_env_copy_raises_error_if_called_with_invalid_path(tmpdir, stupid_env):
    file = Path(tmpdir) / 'file'
    file.touch()
    with stupid_env as env:
        pytest.raises(ValueError, lambda: env.copy_to(file, '../file'))


def test_stupid_env_file_exists_works_with_nonexistent_files(stupid_env):
    with stupid_env as env:
        assert env.file_exists('kokoko') == False


def test_stupid_env_leaves_dirs_after_with(tmpdir):
    env_dir = Path(tmpdir) / 'env'
    with StupidEnv(env_dir, temp_dir=False):
        pass
    assert env_dir.is_dir()