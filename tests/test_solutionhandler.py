"""
This module represent test environment of SolutionHandler class
"""
import os
import random

import pytest

import config
from backend.gateapi.solution import Solution
from backend.solutionhandler import SolutionHandler


def read_test_solution(solution_filename):
    return open(os.path.join(os.path.dirname(
        os.path.abspath(__file__)),
        'solutions',
        solution_filename), 'r').read()


def get_solution(sid=random.randint(-10, 10),
                 acm=False, bonus=0,
                 compiler_id='Python',
                 input_file='a.in', output_file='o.out',
                 problem_id='4',
                 solution_filename='truestring.correct.py',
                 memory_limit=2500,
                 time_limit=1000,
                 test=None):
    if test is None:
        test = [2, 7]
    source = read_test_solution(solution_filename)
    return Solution(sid, acm, bonus, compiler_id, input_file, output_file, problem_id,
                    source, memory_limit, time_limit, test)


@pytest.mark.parametrize("solution, true_result", [
    (
            get_solution(problem_id='5', solution_filename='truestring_correct.c', compiler_id='G++',
                         test=[500, 500]),
            {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    ),
    (
            get_solution(problem_id='6', solution_filename='truestring_correct.c', compiler_id='G++',
                         test=[500, 500]),
            {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    ), (
            get_solution(problem_id='9', solution_filename='truestring_correct.c', compiler_id='G++',
                         test=[500, 500]),
            {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    ),
    (
            get_solution(problem_id='7', solution_filename='same_int_correct.cs', compiler_id='C#',
                         test=[500, 500, 500]),
            {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    )

    ,
    (
            get_solution(problem_id='2', solution_filename='truestring_correct.c', compiler_id='G++', test=[37, 50]),
            {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    ),
    (
            get_solution(problem_id='3', solution_filename='truestring_correct.c', compiler_id='G++', test=[37, 50]),
            {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    ),
    (
            get_solution(bonus=99, compiler_id='G++',
                         solution_filename='truestring_correct.c', problem_id='4'),
            {
                'points': 108,

                'tests': [('OK', 0, None), ('OK', 1, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(bonus=500,
                         solution_filename='truestring_correct.py', test=[8000, 0]), {
                'points': 8500,

                'tests': [('OK', 0, None), ('OK', 1, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(sid=random.randint(-5000, 2), bonus=500, problem_id='4',
                         solution_filename='truestring_incorrect.py', test=[237, 10]), {
                'points': 0,

                'tests': [('WA', 0, None), ('WA', 1, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(acm=True, bonus=500, problem_id='-5э',
                         solution_filename='same_int_correct.py', test=[237, 10]), {
                'points': 747,

                'tests': [('OK', 0, None), ('OK', 1, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(sid=777, acm=True, bonus=500, problem_id='8',
                         solution_filename='minusone_incorrect.py', test=[237, 10, 500]), {
                'points': 237,
                'tests': [('OK', 0, None), ('WA', 1, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ), (
            get_solution(sid=777, compiler_id='C#', problem_id='-5э', bonus=500,
                         solution_filename='same_int_correct.cs', test=[237, 10]), {
                'points': 747,
                'tests': [('OK', 0, None), ('OK', 1, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(sid=777, bonus=500, compiler_id='C#', problem_id='8',
                         solution_filename='minusone_incorrect.cs', test=[237, 10, 50]), {
                'points': 237,
                'tests': [('OK', 0, None), ('WA', 1, None), ('WA', 2, None)],
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(bonus=500, compiler_id='C#', problem_id='8',
                         solution_filename='same_int_ce.cs', test=[237, 10]), {
                'points': 0,
                'compilation_error': True,
                'system_crash': False
            }
    ),
    (
            get_solution(sid=777, bonus=5000000000,
                         solution_filename='same_int_incorrect.cs',
                         compiler_id='C#', problem_id='8', test=[297, 10, 50]), {
                'points': 297,
                'compilation_error': False,
                'system_crash': False
            }
    ),
    (
            get_solution(sid=777, bonus=5000000000,
                         solution_filename='same_int_incorrect.cs',
                         compiler_id='C#', problem_id='10', test=[297, 10, 50]), {
                'points': 0,
                'compilation_error': False,
                'system_crash': True
            }
    )

], ids=[
    'second test is folder (incorrect)',
    'no second test (incorrect)',
    '1abc.tst',
    'random 0 in name (correct)',
    '4 truestring_correct.c no second test',
    '4 truestring_correct.c no second answer',
    '4 truestring_correct.c',
    '4 truestring_correct.py',
    '4 truestring_incorrect.py',
    '-5э same_int_correct.py',
    '8 minusone_incorrect.py',
    '-5e same_int_correct.cs',
    '8 minusone_incorrect.cs',
    '8 same_int_ce.cs',
    '8 same_int_incorrect.cs',
    '01.tst.ans'
])
def test_solution_handler(solution: Solution, true_result):
    """
    Main function for tests
    """
    handler = SolutionHandler(solution, config)
    result = handler.test_solution()

    def remove_output(tests):
        return [t[:2] for t in tests]

    assert result.system_crash == true_result['system_crash']
    assert result.compilation_error == true_result['compilation_error']

    if 'tests' in true_result:
        assert remove_output(result.tests) == remove_output(true_result['tests'])
    assert result.points == true_result['points']
