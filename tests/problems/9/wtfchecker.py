import sys
true_answer = list(map(str.strip, open(sys.argv[2], 'r').readlines()))
answer = list(map(str.strip, open(sys.argv[3], 'r').readlines()))

if true_answer != answer:
    exit(1)
exit(0)
