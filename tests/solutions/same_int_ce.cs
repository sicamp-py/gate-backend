using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        StreamReader sr = new StreamReader("a.in");
        StreamWriter sw = new StreamWriter("o.out");

        sw.WriteLine(sr.ReadLine()-1);
        sr.Close();
        sw.Close();
    }
}