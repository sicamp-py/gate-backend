using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        StreamReader sr = new StreamReader("a.in");
        int number = Convert.ToInt32(sr.ReadLine());
        StreamWriter sw = new StreamWriter("o.out");

        sw.WriteLine(number - 1);
        sr.Close();
        sw.Close();
    }
}