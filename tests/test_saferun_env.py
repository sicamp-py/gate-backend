from backend.environment import SafeRunEnv
from pathlib import Path
import pytest

@pytest.fixture()
def env(tmpdir):
    return SafeRunEnv(Path(tmpdir) / 'saferun_env')

def test_saferunenv_binds_usr(env: SafeRunEnv):
    with env:
        assert env.directory_exists('usr') == True

def test_runs_python(env):
    with env:
        result = env.run(['python', '-c', 'exit(0)'], seccomp=True)
    assert result.is_successful()

def test_run_returns_right_exit_code(env):
    with env:
        result = env.run(['python', '-c', 'exit(1)'], seccomp=True)
    assert result.exit_code == 1

