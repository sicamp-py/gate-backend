from backend.uploading_backend import UploadingBackend
import pytest
from unittest.mock import Mock
from config import PROJECT_DIR
from pathlib import Path

from backend.gateapi.problem import Problem

@pytest.fixture()
def uploading_backend(tmpdir):
    gate = Mock()
    problems = Path(tmpdir) / 'problems'
    checkers = Path(tmpdir) / 'checkers'
    problems.mkdir()
    checkers.mkdir()
    return UploadingBackend(gate,
                            gate_archive_dir=Path(PROJECT_DIR) / 'tests' / 'problems_archives',
                            tests_dir=problems,
                            checkers_dir=checkers)


def test_unpack_problem_unpacks_archive(uploading_backend):
    problem = Problem.from_data({
        'ID': '1',
        'FILENAME': '1.zip',
        'CHECKER': 1
    })
    path_to_tests = Path(uploading_backend.tests_dir) / problem.p_id
    uploading_backend._unpack_problem(problem, path_to_tests)
    assert path_to_tests.is_dir()
    assert (path_to_tests / '1.tst').is_file()
    assert (path_to_tests / '1.ans').is_file()


def test_unpack_problem_works_if_called_two_times_on_same_dir_and_with_same_id(tmpdir, uploading_backend):
    problem = Problem.from_data({
        'ID': '1',
        'FILENAME': '1.zip',
        'CHECKER': '1'
    })
    tmpdir = Path(tmpdir)
    uploading_backend._unpack_problem(problem, tmpdir)
    uploading_backend._unpack_problem(problem, tmpdir)


def test_upload_checker_uploads_and_compiles_simple_cxx_checker(uploading_backend):
    checker = {
        'ID': '1',
        'SRC': 'int main() {}',
        'COMPILERID': 'G++_checker'
    }
    uploading_backend.upload_checker(checker)
    uploading_backend.gate.put_checker.assert_called_with('1')


def test_upload_checker_doesnt_raise_exception_if_called_with_unknown_compiler(uploading_backend):
    checker = {
        'ID': '1',
        'SRC': '[->+<]',
        'COMPILERID': 'bfck'
    }
    uploading_backend.upload_checker(checker)
    uploading_backend.gate.put_checker.assert_called_with('1', False, message='Unsupported compiler for checkers: bfck')


def test_upload_problem_puts_correct_message_if_called_with_nonexistent_archive(uploading_backend):
    problem = Problem.from_data({
        'ID': '1',
        'FILENAME': 'kokoko.zip',
        'CHECKER': '1'
    })
    uploading_backend.upload_problem(problem)
    uploading_backend.gate.put_problem.assert_called_with('1', False,
                                                          message='Invalid archive path, probably problem in config: '
                                                                  'kokoko.zip')


def test_upload_problem_puts_correct_message_if_called_with_invalid_archive(uploading_backend):
    problem = Problem.from_data({
        'ID': '1',
        'FILENAME': 'invalidarchive.zip',
        'CHECKER': '1'
    })
    uploading_backend.upload_problem(problem)
    uploading_backend.gate.put_problem.assert_called_with('1', False,
                                                          message='Invalid or unsupported archive: invalidarchive.zip')