import backend.gateapi as gateapi
from backend.gateapi.gateapiclient import format_testing_result_as_dict


class Test_FormatTestingResult:

    def test_ce(self):
        result = gateapi.TestingResult()
        result.ce("msg")
        formatted = format_testing_result_as_dict(result)
        assert formatted == {'POINTS': 0, 'COMPILER_MESSAGES': 'msg', 'ERRORS': 'CE',
                             'TESTS': '', 'SOLUTION_OUTPUT': '', 'CHECKER_OUTPUT': '', 'REPORT': ''}
