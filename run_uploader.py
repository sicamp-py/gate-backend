from backend.gateapi import GateApiClient
from backend.uploading_backend import UploadingBackend


def make_backend(config) -> UploadingBackend:
    gate_api = GateApiClient(config.gate['tester_url'],
                             config.gate['login'],
                             config.gate['pass1'],
                             config.gate['pass2'])

    return UploadingBackend(gate_api,
                            gate_archive_dir=config.dirs['gate_archive_dir'],
                            tests_dir=config.dirs['tests_dir'],
                            checkers_dir=config.dirs['checkers_dir'],
                            delay_between_requests=config.DELAY_BETWEEN_REQUESTS)


if __name__ == '__main__':
    import config
    backend = make_backend(config)
    backend.main_loop()
